/**
 * АВТОМАТИЧЕСКИЙ ГОД
 *
 * Чтобы автогод заработал, нужно добавить в строку копирайта или в другую
 * <span class="str_autoyear" data-start="*год запуска сервиса*"></span>.
 * При загрузке страницы код будет смотреть на текущий год
 * и если он совпадает с годом старта, то выведет только текущий год.
 * Если год старта меньше текущего, то выведется интервал "2001–2016".
 * Если год старта больше текущего, то выведется текущий год.
 */

(function() {
  if (!document.querySelectorAll('span.str_autoyear')) return;
  var autoYear = {
    nodes: document.querySelectorAll('span.str_autoyear'),
    thisyear: new Date().getFullYear(),

    start: function (i) { return Number(this.nodes[i].dataset.start); },

    setYear: function () {
      for (var i = 0; i < this.nodes.length; i++) {
        this.nodes[i].innerHTML = this.start(i) < this.thisyear ? this.start(i) + '&thinsp;&ndash;&thinsp;' + this.thisyear : this.thisyear;
      }
    }
  };
  autoYear.setYear();
})();